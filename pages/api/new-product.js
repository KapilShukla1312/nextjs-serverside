import { MongoClient } from "mongodb";

async function handler(req, res) {
  if (req.method === "POST") {
    const data = req.body;

    console.log(data);

    const client = await MongoClient.connect(
      "mongodb+srv://kapilnext:kapil8826@cluster0.cx9mg.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
    );

    const db = client.db();

    const cartCollection = db.collection("product");

    const result = await cartCollection.insertOne(data);

    console.log(result);

    client.close();

    res.status(201).json({ message: "product inserted!" });
  }
}

export default handler;
