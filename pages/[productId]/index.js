import CartDetail from "../../components/cartups/CartDetail";
import { MongoClient, ObjectId } from "mongodb";

function productDetail(props) {
  console.log(props.cartData);
  return (
    <CartDetail
      image={props.cartData.image}
      name={props.cartData.name}
      price={props.cartData.price}
      description={props.cartData.description}
    />
  );
}

export async function getStaticPaths() {
  const client = await MongoClient.connect(
    "mongodb+srv://kapilnext:kapil8826@cluster0.cx9mg.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
  );

  const db = client.db();

  const cartProduct = db.collection("product");

  const productData = await cartProduct.find({}, { _id: 1 }).toArray();

  client.close();

  return {
    fallback: false,
    paths: productData.map((item) => ({
      params: {
        productId: item._id.toString(),
      },
    })),
  };
}

export async function getStaticProps(context) {
  const cartId = context.params.productId;

  const client = await MongoClient.connect(
    "mongodb+srv://kapilnext:kapil8826@cluster0.cx9mg.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
  );

  const db = client.db();

  const cartProduct = db.collection("product");

  const productData = await cartProduct.findOne({
    _id: ObjectId(cartId),
  });

  client.close();

  return {
    props: {

      cartData: {
        id: productData._id.toString(),
        image: productData.image,
        name: productData.title,
        price: productData.price,
        description: productData.description,
      }
    },
  };
}

export default productDetail;
