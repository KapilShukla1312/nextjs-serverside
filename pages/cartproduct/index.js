import { useRouter } from "next/router";
import NewCartForm from "../../components/cartups/NewCartForm";

function NewProduct() {
  const router = useRouter();

  async function addProductHandler(enteredMeetupData) {
    console.log(enteredMeetupData);
    const response = await fetch("/api/new-product", {
      method: "POST",
      body: JSON.stringify(enteredMeetupData),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await response.json();

    console.log(enteredMeetupData);

    console.log(data);

    router.push("/");
  }

  return <NewCartForm onAddProduct={addProductHandler} />;
}

export default NewProduct;
