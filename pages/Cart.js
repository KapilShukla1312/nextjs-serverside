import { useSelector } from "react-redux";
import CartDetail from "../components/cartups/CartDetail";

function Cart() {
  const getData = useSelector((state) => state.cart.cartNumber);

  return (
    <div>
      {getData.map((item, index) => (
        <div key={index}>
          <CartDetail
            image={item.image}
            name={item.name}
            price={item.price}
            description={item.description}
          />
        </div>
      ))}
    </div>
  );
}

export default Cart;
