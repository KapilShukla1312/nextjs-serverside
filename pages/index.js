import { Fragment } from "react";
import CartList from "../components/cartups/CartList";
import { MongoClient } from "mongodb";
import classes from "../components/index.module.css";

function Home(props) {
  return (
    <div className={classes.container}>
      <CartList products={props.cartData} />
    </div>
  );
}

export async function getServerSideProps() {
  const client = await MongoClient.connect(
    "mongodb+srv://kapilnext:kapil8826@cluster0.cx9mg.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
  );

  const db = client.db();

  const cartProduct = db.collection("product");

  const productData = await cartProduct.find().toArray();

  client.close();

  return {
    props: {
      cartData: productData.map((item) => ({
        name: item.title,
        price: item.price,
        image: item.image,
        id: item._id.toString(),
      })),
    },
  };
}


export default Home;
