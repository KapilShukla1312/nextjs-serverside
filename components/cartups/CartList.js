import CartItem from "./CartItem";
import classes from "./CartList.module.css";

function CartList(props) {
  return (
    <ul className={classes.list}>
      {props.products.map((Cart) => (
        <CartItem
          key={Cart.id}
          id={Cart.id}
          image={Cart.image}
          name={Cart.name}
          price={Cart.price}
          description={Cart.description}
        />
      ))}
    </ul>
  );
}

export default CartList;
