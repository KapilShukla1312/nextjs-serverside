import classes from "./CartDetail.module.css";

function CartDetail(props) {
  return (
    <section className={classes.detail}>
      <img src={props.image} alt={props.name} />
      <h1>{props.name}</h1>
      <address>{props.price}</address>
      <p>{props.description}</p>
    </section>
  );
}

export default CartDetail;
