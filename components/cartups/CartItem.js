import { useRouter } from "next/router";
import Card from "../ui/Card";
import classes from "./CartItem.module.css";
import { useDispatch } from "react-redux";
import { cartAction } from "../../redux/actions/Cart";

function CartItem(props) {
  const router = useRouter();

  const disptach = useDispatch();

  const showDetailRouter = () => {
    router.push("/" + props.id);
  };

  const handleClick = () => {
    disptach(cartAction(props));
  };

  return (
    <li className={classes.item}>
      <Card>
        <div className={classes.image}>
          <img src={props.image} alt={props.title} />
        </div>
        <div className={classes.content}>
          <h3>{props.name}</h3>
          <address>{props.price}</address>
          <p>{props.description}</p>
        </div>
        <div className={classes.buttonGroup}>
          <div className={classes.actions}>
            <button onClick={() => handleClick(props)}>Add to Cart</button>
          </div>
          <div className={classes.actions}>
            <button onClick={showDetailRouter}>Show Details</button>
          </div>
        </div>
      </Card>
    </li>
  );
}

export default CartItem;
