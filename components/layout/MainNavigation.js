import classes from "./MainNavigation.module.css";
import Link from "next/link";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Badge from "@material-ui/core/Badge";
import { useSelector } from "react-redux";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";

function MainNavigation(props) {
  const getData = useSelector((state) => state.cart.cartNumber); // for updating the cart badge number

  // console.log("getData --->>", getData);

  return (
    <header className={classes.header}>
      <Link href="/">
        <div className={classes.logo}>Home</div>
      </Link>
      <nav className={classes.navbar}>
        <Link href="/Cart">
          <Badge badgeContent={getData.length} color="primary">
            <ShoppingCartIcon style={{ fontSize: 25, color: "#fff" }} />
          </Badge>
        </Link>
        <ul>
          <li>
            <Link href="/cartproduct">
              <AddShoppingCartIcon style={{ fontSize: 40, color: "#fff" }} />
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

// export async function getStaticProps() {
//   const client = await MongoClient.connect(
//     "mongodb+srv://kapilnext:kapil8826@cluster0.cx9mg.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
//   );

//   const db = client.db();

//   const cartProduct = db.collection("product");

//   const productData = await cartProduct.find().toArray();

//   client.close();
//   return {
//     props: {
//       cartData: productData,
//     },
//     revalidate: 1,
//   };
// }

export default MainNavigation;
